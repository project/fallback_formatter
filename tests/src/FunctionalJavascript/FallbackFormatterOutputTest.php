<?php

namespace Drupal\Tests\fallback_formatter\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test basic functionality of the fallback formatter.
 *
 * @group fallback_formatter
 */
class FallbackFormatterOutputTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views',
    'views_ui',
    'field_ui',
    'node',
    'fallback_formatter',
    'fallback_formatter_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    $this->adminUser = $this->drupalCreateUser([
      'administer views',
      'create article content',
      'access content overview',
      'administer node display',
    ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests fallback formatter settings in a view.
   */
  public function testFormatterSettingsInView() {
    // Create new article.
    $this->drupalGet('node/add/article');
    $page = $this->getSession()->getPage();
    $page->fillField('Title', $this->randomMachineName());
    $page->pressButton('Save');

    // Add a new field in content overview with the author.
    $this->drupalGet('admin/structure/views/nojs/add-handler/content/page_1/field');
    $page = $this->getSession()->getPage();
    $page->fillField('name[node_field_data.uid]', TRUE);
    $page->pressButton('Add and configure fields');
    $this->assertSession()->optionExists('options[type]', 'fallback');

    // Set fallback formatter as the formatter for the new field.
    $page = $this->getSession()->getPage();
    $page->fillField('Formatter', 'fallback');
    $page->pressButton('Apply');

    // Set fallback formatter as the formatter for the new field.
    $this->drupalGet('admin/structure/views/nojs/handler/content/page_1/field/uid');
    $page = $this->getSession()->getPage();
    $page->checkField('fallback_formatter_settings[formatters][entity_reference_label][status]');
    $page->pressButton('Apply');
    $this->drupalGet('admin/structure/views/nojs/handler/content/page_1/field/uid');
    $this->assertSession()->checkboxChecked('edit-fallback-formatter-settings-formatters-entity-reference-label-status');
    $this->assertSession()->checkboxNotChecked('edit-fallback-formatter-settings-formatters-author-status');
    // Apply the changes.
    $page = $this->getSession()->getPage();
    $page->pressButton('Apply');
    $page->pressButton('Save');

    // Check the output of the formatter.
    $this->drupalGet('admin/content');
    $this->assertSession()->pageTextContains($this->adminUser->getDisplayName());
  }

  /**
   * Tests fallback formatter settings in managed display.
   */
  public function testFormatterSettingsInManageDisplay() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $this->drupalGet('admin/structure/types/manage/article/display');
    $this->assertSession()->optionExists('fields[body][type]', 'fallback');
    $page->selectFieldOption('fields[body][type]', 'fallback');
    $this->assertNotEmpty($this->assertSession()->waitForText('No formatters selected yet.'));
    $page->pressButton('Save');
    $page->pressButton('body_settings_edit');
    $checkbox = $assert_session->waitForField('fallback_formatter_settings[formatters][text_trimmed][status]');
    $this->assertNotEmpty($checkbox);
    $checkbox->check();
    $page->pressButton('Update');
    $assert_session->pageTextContains('Trimmed');
  }

}
